const { src, dest, watch } = require('gulp');
const sass = require('gulp-sass');

function compile_sass(){
    return  src("./scss/**/*.scss")
        .pipe(sass())
        .pipe(dest('./public/stylesheets/'));
}

exports.default = function(){
    watch("./scss/**/*.scss", compile_sass);
}