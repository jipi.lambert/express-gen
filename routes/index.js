var express = require('express');
var router = express.Router();
let what = require("../custom_modules/what.js");
require ("../custom_modules/sum.js");

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express', exemple:'Test moi ca', sum: sum(2,3) });
});

router.get('/what', function(req,res, next){
  res.render('what', {what:what()});
});

router.get('/contact', function(req,res,next){
  res.render('contact');
});

router.post('/contact', function(req,res,next){
  console.log(req.body);
  res.render('contact', {reponse:req.body});
});

module.exports = router;
